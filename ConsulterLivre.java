package Exo4biblio;

import java.util.List;

public class ConsulterLivre {

	public static void consulter(List<Livre> listeDesLivres) {
		int boucle = 0;
		for(Livre num : listeDesLivres) {
			boucle++;
			Affichage.afficher(num.getTitre(), boucle);
		}
	}
}