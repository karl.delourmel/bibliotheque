package Exo4biblio;

import java.util.ArrayList;
import java.util.List;

public class Main {
		
	public static void main(String[] arg) {
		
		List<Livre> listeDesLivres = new ArrayList<>();
		listeDesLivres.add(new Roman("Numéro deux", "David Foenkinos", 2022));
		listeDesLivres.add(new Bd("Astérix chez les Bretons", "René Goscinny", 1966, "Albert Uberzo"));
		listeDesLivres.add(new Bd("La Nuit", "Philippe Druillet", 1976, "Philippe Druillet"));
		
		Communication.output("Bienvenu dans votre Bibliothèque");
		String choix;
			do {
				choix = "";
				Communication.output("Que voulez-vous faire ? \n ajouter, enlever, consulter, lire ou quitter : ");
				choix = Communication.input();
				
				switch(choix){ 
					case "ajouter":
						Livre livreNew = ajouterLivre.ajoutLivre();
						listeDesLivres.add(livreNew);
						break;
					case "enlever": // tester avec xxx.forEach(->) Java 7 page 35
						SupprimeLivre.supprimeLivre(listeDesLivres);
						break;
					case "consulter":
						ConsulterLivre.consulter(listeDesLivres);
						break;
					case "lire": 
						LireLivre.lire(listeDesLivres);
						break;
					case "quitter":
						Communication.output("A bientôt");
						choix = null;
						break;
					default: 
						Communication.output("Saisie Inconnu");
						break;
				}
			} while(null != choix);
	}
}