package Exo4biblio;

public class Affichage {

	public static void afficher(String titre, String auteur, int publication) {
		Communication.output("Roman : " + titre + "\n" + "Auteur : " + auteur + "\n" + "Année : " + publication + "\n");
	}
	public static void afficher(String titre, String auteur, int publication, String dessin) {
		Communication.output("Bd : " + titre + "\n" + "Texte : " + auteur + "\n" + "Dessin : " + dessin + "\n" + "Année : " + publication + "\n");
	}
	public static void afficher(String titre, int numero) {
		Communication.output("Livre n°" + numero + " : " + titre);
	}
	public static void afficher() {
		Communication.output("erreur affichage");
	}
}