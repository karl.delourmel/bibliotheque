package Exo4biblio;

import java.util.List;

public interface LivreCrud {
	
	Livre getAjoutLivre(Livre livre);
	
	List<Livre> getAllLivre();
	
	Livre ajoutLivre(Livre livre);
	
	Livre SupprimeLivre(Livre livre);
	
	Livre modificationLivre(Livre livre);
	
}