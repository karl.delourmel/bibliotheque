package Exo4biblio;

import java.util.List;

public class LireLivre {
	
	public static void lire(List<Livre> listeDesLivres) {
		
		Communication.output("Que voulez-vous lire ? (tapper le titre) : ");
		String saisieTitre = Communication.input();

		for(Livre num : listeDesLivres) {
			if (saisieTitre.equals(num.getTitre()) && (num.getDessin() != null)) {
				Affichage.afficher(num.getTitre(), num.getAuteur(), num.getPublication(), num.getDessin());
			} else if (saisieTitre.equals(num.getTitre())){
				Affichage.afficher(num.getTitre(), num.getAuteur(), num.getPublication());	
			} else {
			}
		}
	}
}