package Exo4biblio;

import java.util.Iterator;
import java.util.List;

public class SupprimeLivre {

	public static void supprimeLivre(List<Livre> listeDesLivres) {
		Communication.output("Quel livre voulez-vous supprimer ? ");
		String titre = Communication.input();
		
		boolean err = false;
		
		Iterator<Livre> iterator = listeDesLivres.iterator();
		while (iterator.hasNext()) { 
			Livre livre = iterator.next();
			if (titre.equals(livre.getTitre())){
				iterator.remove();
				Communication.output("Livre ' " + livre.getTitre() + " ' enlevé.");
				err = true;
			} 	
		}
		
		if (false == err) {
		Communication.output("Livre non trouvé");
		}
	}
}