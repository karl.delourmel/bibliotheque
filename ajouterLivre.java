package Exo4biblio;

public class ajouterLivre {

	public static Livre ajoutLivre() {

		Communication.output("Ajout d'un Roman");
		Communication.output("Titre : ");
		String titre = Communication.input();
		Communication.output("Auteur : ");
		String auteur = Communication.input();
		Communication.output("Année de Publication : ");
		int publication = Integer.parseInt(Communication.input());
		Livre livreRoman = new Roman(titre, auteur, publication);
		return livreRoman;	
	}
}	