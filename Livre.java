package Exo4biblio;

import java.io.Serializable;

public abstract class Livre implements Serializable {

	private static final long serialVersionUID = 1L;
	private String titre;
	private String auteur;
	private int publication;
	
	public Livre(String titre, String auteur, int publication) {
		this.titre = titre;
		this.auteur = auteur;
		this.publication = publication;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public int getPublication() {
		return publication;
	}

	public void setPublication(int publication) {
		this.publication = publication;
	}
	
	public abstract String getDessin();
}
