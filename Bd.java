package Exo4biblio;

public class Bd extends Livre{
	
	private static final long serialVersionUID = 1L;
	private String dessin;

	public Bd(String titre, String texte, int publication, String dessin) {
		super(titre, texte, publication);
		this.dessin = dessin;
	}

	@Override
	public String getDessin() {
		return dessin;
	}
}