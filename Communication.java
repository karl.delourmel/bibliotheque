package Exo4biblio;

import java.util.Scanner;

public class Communication implements IInput, IOutput{
	
	private static Scanner scSaisie;

	public static String input() {
		scSaisie = new Scanner(System.in);
		try {
			return scSaisie.nextLine();
		} catch (Exception e) {
			System.err.println("Erreur lors de la saisie : " + e.getMessage());
			return null;
		}
	}
	
	public void closeScanner() {
		if (null != scSaisie) {
			scSaisie.close();
		}
	}
	
	public static void output(String message) {
		System.out.println(message);
	}
}